let express = require('express'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    methodOverride = require('method-override'),
    schedule = require('node-schedule'),
    mysql = require('mysql')
app = express();

const createLocationList = require('./lib/createLocations/compileLocationList.js'),
    getLocations = require('./lib/createLocations/getLocations.js'),
    getMarketOrders = require('./lib/aggregateMarketOrders/getMarketOrders.js'),
    tranquilityStatus = require('./pixieLair/statusPixie')
locationRoute = require('./routes/location');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(methodOverride('_method'));
app.use(express.static(__dirname + '/public'));


//DBs n stuff with data
mongoose.connect("mongodb://localhost/evemarketserver", {
    useNewUrlParser: true
});

const mysqlDB = mysql.createConnection({
    host: '127.0.0.1',
    port: 3306,
    user: 'root',
    password: '',
    database: 'evemarketserver'
});

mysqlDB.connect((err) => {
    if (err) {
        console.error('error connecting to mysql db: ' + mysqlDB.threadId);

        return;
    }
    console.log('connected to mysql db as id ' + mysqlDB.threadId);
});



let locationTimer = ['*', 15, 12, '*', '*', '*'],
    orderUpdateTimer = ['*', 15, 11, '*', '*', '*'];

function locationUpdate(locationTimer) {
    schedule.scheduleJob(locationTimer.toString().replace(/,/g, ' '), () => {
        let tqStatus = tranquilityStatus();
        if (tqStatus) {
            locationTimer = ['*', 15, 12, '*', '*', '*'];
            createLocationList();
        } else {
            locationTimer[1] += 1;
            locationTimer[2] = '*';
            console.log('TQ is offline will try again 1 minute.');
            locationUpdate(locationTimer);
        }
    });

}
async function test(){
    /*
    let queryDev = mysqlDB.query('TRUNCATE TABLE marketorders', (err, res, fields) => {
        if (err) throw err;
    });
    queryDev.sql;
*/
    const location = new getLocations();
    await location.getOldRegions();  
    
    //await getMarketOrders(10000002);
   
    for (let region of location.regionList) {
       await getMarketOrders(region);
    }
    
}
//test();

locationUpdate(locationTimer);
app.use('/locations', locationRoute);

app.listen(3000, () => {
    console.log('contact');
});