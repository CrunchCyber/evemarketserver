let mongoose = require('mongoose');

let orderSchema = new mongoose.Schema({
    region_id: Number,
    duration: Number,
    is_buy_order: Boolean,
    issued: Date,
    location_id: Number,
    price: Number,
    rang: String,
    system_id: Number,
    type_id: Number,
    volume_remain: Number,
    volume_total: Number
});

module.exports = mongoose.model("MarketOrder", orderSchema);