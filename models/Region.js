let mongoose        = require('mongoose');

let regionSchema = new mongoose.Schema({
    regionID: Number,
    name: String,
    constellations: [
        {
            constellationID: Number,
            name: String,
            systems: [
                {
                    systemID: Number,
                    name: String,
                    stations: [
                        {
                            name: String,
                            stationID: Number
                        }
                    ]
                }      
            ]
        }       
    ]
});

module.exports = mongoose.model("Region", regionSchema);