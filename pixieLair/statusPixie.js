const fetch = require("node-fetch");

module.exports = async () => {
    const url = 'https://esi.evetech.net/latest/status/?datasource=tranquility';
    return fetch(url)
            .then((response) => {
                if (response.status === 200 && response.ok) {
                    return response.json();
                } else return fals
            })
            .then((json) => {
                if(json.players < 15 || json.vip === true){
                    return false;
                }else{
                    return true;
                }
            })
            .catch(err => console.log(err));        
}