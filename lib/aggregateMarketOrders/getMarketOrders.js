const fetch = require("node-fetch"),
    mysql = require('mysql'),
    MarketOrder = require('../../models/MarketOrder.js');
/*
const mysqlDB = mysql.createConnection({
    host: '127.0.0.1',
    port: 3306,
    user: 'root',
    password: '',
    database: 'evemarketserver'
});



mysqlDB.connect((err) => {
    if (err) {
        console.error('error connecting to mysql db: ' + mysqlDB.threadId);

        return;
    }
    console.log('connected to mysql db as id ' + mysqlDB.threadId);
});
*/

module.exports = async (region) => {
    let notEmpty = true;
    let page = 0;
    console.log('Processing: ' + region);

    while (notEmpty) {
        page++;
        fancyPointer = 0;
        let searchQuery = `https://esi.evetech.net/latest/markets/${region}/orders/?datasource=tranquility&order_type=all&page=${page}`;
        let res = await orderQuery(searchQuery);
        console.log("I loaded res at " + Date.now());

        if (Object.entries(res).length === 0 /*&& res.constructor === Object*/ ) {
            notEmpty = false;
        } else {
            writeToMongo(res, region);
            //writeToDatabase(res, region);
        }
        console.log(page + '\t' + notEmpty);
    }
};


function writeToMongo(res, region) {

    for (let entry of res) {
        entry.region_id = region;
        MarketOrder.create(entry, (err, newDB) => {
            if (err) {
                console.log(err);
            } else {
                // console.log(newDB);
                console.log('Entry Added');
            }
        });
    }
}



/*
//remember the region
async function writeToDatabase(res, region) {
    // console.log(fancyPointer);
    let counter = 0;
        for(let entry of res){
             mysqlDB.query('INSERT INTO marketorders SET ?', entry, (err, result, fields) => {
                if (err) throw err;                     
                             
            });
  
        }
       
};
*/
async function orderQuery(searchQuery) {
    return fetch(searchQuery)
        .then((response) => {
            if (response.status === 200 && response.ok) {
                return response.json();
            } else {
                orderQuery(searchQuery);
            }
        })
        .then((json) => {
            if (json === null) {
                return json;
            } else {
                for (let entry of json) {
                    entry.location = entry.range;
                    delete entry.range;
                }
                return json;
            }
        })
        .catch(err => console.log(err));
};