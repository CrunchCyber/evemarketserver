const queries = [
    ['https://esi.evetech.net/latest/universe/regions/?datasource=tranquility'],
    ['https://esi.evetech.net/latest/universe/regions/', '/?datasource=tranquility'],
    ['https://esi.evetech.net/latest/universe/constellations/', '/?datasource=tranquility'],
    ['https://esi.evetech.net/latest/universe/systems/', '/?datasource=tranquility'],
    ['https://esi.evetech.net/latest/universe/stations/', '/?datasource=tranquility']
]

module.exports = queries;