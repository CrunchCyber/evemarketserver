queries = require('./locationStrings');
const fetch = require("node-fetch");

class Locations {
    constructor() {
        this.retries = 0;
    }

    async getRegionList() {
        this.regionList = [];
        const res = await locQuery(-1, 0);

        this.regionList = res;
    }

    async getOldRegions() {
        this.regionList = [];
        const res = await locQuery(-1, 0);
        for (let data of res) {
            let dataString = data.toString();
            if (dataString[1] === '0') {
                this.regionList.push(data);
            }
        }
    }

    async getRegionDetails(list) {
        let tempRegionList = [];
        for (let id of list) {
            let region = {};
            region.ID = id;

            let res = await locQuery(id, 1);
            if (res === undefined) {
                res = await locQuery(id, 1);
            }
            region.name = res.name;

            region.constellations = res.constellations;
            tempRegionList.push(region);
        }
        this.regionList = tempRegionList;
    }

    async getConstellationList(list) {
        let tempConstellationsList = [];
        for (let id of list) {
            let constellation = {};
            constellation.ID = id;
            let res = await locQuery(id, 2);
            if (res === undefined) {
                res = await locQuery(id, 2);
            }
            constellation.name = res.name;
            constellation.systems = res.systems;
            tempConstellationsList.push(constellation);

        }
        this.constellationList = tempConstellationsList;
    }

    async getSystemList(list) {
        let tempSystemList = [];
        for (let id of list) {
            let system = {};
            system.ID = id;
            let res = await locQuery(id, 3);
            if (res === undefined) {
                res = await locQuery(id, 3);
            }
            system.name = res.name;
            system.stations = res.stations;
            tempSystemList.push(system);
        }
        this.systemList = tempSystemList;
    }

    async getStationList(list) {
        let tempStationList = [];
        for (let id of list) {
            let res = await locQuery(id, 4);
            if (res === undefined) {
                res = await locQuery(id, 4);
            }
            const station = {
                'name': res.name,
                'ID': id
            }
            tempStationList.push(station);
        }
        this.stationList = tempStationList;
    }
};

async function locQuery(id, type){
    let searchQuery;
    if (id === -1) {
        searchQuery = queries[type].toString();
    } else {
        searchQuery = queries[type][0].toString() + id + queries[type][1].toString();
    }
    return fetch(searchQuery)
        .then((response) => {
            if (response.status === 200 && response.ok) {
                return response.json();
            } else /*if (response.status === 502 && response.ok)*/ {
                locQuery(id, type);
            }
        })
        .then((json) => {
            return json;
        })
        .catch(err => console.log(err));

}

module.exports = Locations;