let locations = require('./getLocations')
Location = require('../../models/Region');

module.exports = async () => {

    location = new locations();

    await location.getOldRegions();
    await location.getRegionDetails(location.regionList);

    for (let region of location.regionList) {
        locationList(region, location);
    };
}

const locationList = async (region, location) => {
    startTime = new Date();
    // console.log('Parsing ' + region.name + '\t\t\t' + Date.now());
    let constellations = [];

    //Get constellations
    // console.log('Parsing Constellations \t\t\t' + Date.now());

    await location.getConstellationList(region.constellations);

    for (let constellation of location.constellationList) {

        let systems = [];

        //Get systems
        // console.log('└' + constellation.name + ' systems \t\t\t' + Date.now());
        await location.getSystemList(constellation.systems);

        for (let system of location.systemList) {
            let stations = [];

            //Get stations                  
            // console.log('└─' + system.name + ' stations \t\t\t' + Date.now());
            if (system.stations !== undefined) {
                await location.getStationList(system.stations);

                for (let station of location.stationList) {
                    // console.log('└──' + station.name + ' station \t ' + station.ID + '\t\t' + Date.now());
                    stations.push({
                        name: station.name,
                        stationID: station.ID
                    });
                }
            }
            systems.push({
                name: system.name,
                systemID: system.ID,
                stations: stations
            });
        }
        constellations.push({
            name: constellation.name,
            constellationID: constellation.ID,
            systems: systems
        });
    }

    let newLocationList = {
        name: region.name,
        regionID: region.ID,
        constellations: constellations
    };
    endTime = new Date();
    console.log('Writing to DB' + region.name);
    console.log('Start: ' + startTime);
    console.log('End: ' + endTime);
    console.log('Duration: ' + (endTime-startTime));
    
    
    Location.deleteOne({name: region.name}).exec(); 
    Location.create(newLocationList, (err, newDB) => {
        if (err) {
            console.log(err);
        } else {
            // console.log(newDB);
            console.log('Done parsing ' + region.name);
        }
    })
    
}