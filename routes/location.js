let Location = require('../models/Region');

let express = require('express'),
    router = express.Router();

//generate object of reigon {name: name, regionID:}
router.get('/', (req,res)=> {
    Location.find((err, result) => {
        let list = [];
        if(err){
            console.log(err);    
        }else{
         for (let entry of result){          
             list.push({'name': entry.name, 'regionID': entry.regionID});          
         }   
        }
        res.send(list);
    });
});

router.get('/:region_id', (req,res) => {
    console.log('Someone asked for ' + req.params.region_id);
        
   result = Location.findOne({'regionID': req.params.region_id}, '-_id', (err, result) => {
       if (err) {
           console.log(err);    
       } else {
        res.send(result);
       }
   });  
});

module.exports = router;
